import 'package:flutter/material.dart';

class ChipWidget extends StatefulWidget {
  final Map<String, Object> choicesList;

  const ChipWidget(this.choicesList, {Key? key}) : super(key: key);

  @override
  State<ChipWidget> createState() => _ChipWidgetState();
}

class _ChipWidgetState extends State<ChipWidget> {
  Map<String, Object>? choicesList;

  @override
  void initState() {
    super.initState();
    choicesList = widget.choicesList;
  }

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      label: Text(
        choicesList!["name"].toString(),
        style: Theme.of(context)
            .textTheme
            .bodyText2!
            .copyWith(color: Colors.white, fontSize: 14),
      ),
      selected: choicesList!['select'] == true,
      selectedColor: Colors.blue,
      onSelected: (value) {
        setState(() {
          if (choicesList!['select'] == true) {
            choicesList!['select'] = false;
          } else {
            choicesList!['select'] = true;
          }
        });
      },
      elevation: 1,
    );
  }
}
