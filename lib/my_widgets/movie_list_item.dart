import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pre_interview/mob_x/movie_mob.dart';
import 'package:pre_interview/routes/app_router.gr.dart';

class MovieListItemWidget extends StatefulWidget {
  final Movie? movie;

  const MovieListItemWidget({Key? key, this.movie}) : super(key: key);

  @override
  State<MovieListItemWidget> createState() => _MovieListItemWidgetState();
}

class _MovieListItemWidgetState extends State<MovieListItemWidget> {
  StackRouter? stackRouter;
  Movie? movie;

  @override
  void initState() {
    super.initState();
    stackRouter = context.router;
    movie = widget.movie;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        stackRouter?.push(CreateMovieRoute(
          id: movie!.id,
        ));
      },
      child: Card(
          margin: const EdgeInsets.only(bottom: 16),
          // elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: const BorderSide(
              color: Colors.black,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Observer(
                    builder: (_) => Row(children: [
                          const Text(
                            "Title: ",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                              color: Colors.black38,
                            ),
                          ),
                          Text(
                            movie!.title!,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ])),
                Observer(
                  builder: (_) => Row(children: [
                    const Text(
                      "Director: ",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        color: Colors.black38,
                      ),
                    ),
                    Text(
                      movie!.director!,
                      style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                Observer(
                    builder: (_) => Text(
                          movie!.summary!,
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                        )),
                const SizedBox(
                  height: 10,
                ),
                Align(
                    alignment: Alignment.topRight,
                    child: Observer(
                        builder: (_) => Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: movie!.genres!
                                  .map(
                                    (e) => Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                        3,
                                        0,
                                        3,
                                        0,
                                      ),
                                      child: Text(
                                        e.name,
                                        textAlign: TextAlign.right,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  )
                                  .toList(),
                            )))
              ],
            ),
          )),
    );
  }
}
