import 'package:auto_route/auto_route.dart';
import 'package:pre_interview/create_movie.dart';
import 'package:pre_interview/dashboard.dart';
import 'package:pre_interview/main.dart';

@MaterialAutoRouter(routes: <AutoRoute>[
  AutoRoute(
    page: MyHomePage,
    initial: true,

  ),
  AutoRoute(
    page: Dashboard,
  ),
  AutoRoute(
    page: CreateMovie,
    path: "/create-movie/:id"
  ),
])
class $AppRouter {}
