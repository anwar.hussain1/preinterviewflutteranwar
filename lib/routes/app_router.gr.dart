// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;

import '../create_movie.dart' as _i3;
import '../dashboard.dart' as _i2;
import '../main.dart' as _i1;

class AppRouter extends _i4.RootStackRouter {
  AppRouter([_i5.GlobalKey<_i5.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    MyHomePageRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.MyHomePage());
    },
    DashboardRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.Dashboard());
    },
    CreateMovieRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<CreateMovieRouteArgs>(
          orElse: () => CreateMovieRouteArgs(id: pathParams.optInt('id')));
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i3.CreateMovie(key: args.key, id: args.id));
    }
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(MyHomePageRoute.name, path: '/'),
        _i4.RouteConfig(DashboardRoute.name, path: '/Dashboard'),
        _i4.RouteConfig(CreateMovieRoute.name, path: '/create-movie/:id')
      ];
}

/// generated route for
/// [_i1.MyHomePage]
class MyHomePageRoute extends _i4.PageRouteInfo<void> {
  const MyHomePageRoute() : super(MyHomePageRoute.name, path: '/');

  static const String name = 'MyHomePageRoute';
}

/// generated route for
/// [_i2.Dashboard]
class DashboardRoute extends _i4.PageRouteInfo<void> {
  const DashboardRoute() : super(DashboardRoute.name, path: '/Dashboard');

  static const String name = 'DashboardRoute';
}

/// generated route for
/// [_i3.CreateMovie]
class CreateMovieRoute extends _i4.PageRouteInfo<CreateMovieRouteArgs> {
  CreateMovieRoute({_i5.Key? key, int? id})
      : super(CreateMovieRoute.name,
            path: '/create-movie/:id',
            args: CreateMovieRouteArgs(key: key, id: id),
            rawPathParams: {'id': id});

  static const String name = 'CreateMovieRoute';
}

class CreateMovieRouteArgs {
  const CreateMovieRouteArgs({this.key, this.id});

  final _i5.Key? key;

  final int? id;

  @override
  String toString() {
    return 'CreateMovieRouteArgs{key: $key, id: $id}';
  }
}
