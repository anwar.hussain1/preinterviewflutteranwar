
import 'package:get_it/get_it.dart';
import 'package:pre_interview/mob_x/movie_list_mob.dart';

final getItProvider = GetIt.instance;

void setup(){
  getItProvider.registerLazySingleton<MovieList>(() => MovieList());
}