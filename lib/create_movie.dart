import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:pre_interview/my_widgets/chip_widget.dart';
import 'package:pre_interview/get_it/locator.dart';
import 'package:pre_interview/models/genre.dart';

import 'mob_x/movie_list_mob.dart';
import 'mob_x/movie_mob.dart';

class CreateMovie extends StatefulWidget {
  final int? id;

  const CreateMovie({Key? key, @PathParam('id') this.id}) : super(key: key);

  @override
  State<CreateMovie> createState() => _CreateMovieState();
}

class _CreateMovieState extends State<CreateMovie> {
  StackRouter? stackRouter;
  int defaultChoiceIndex = 0;
  int? id;
  Movie? movie;
  String message = "";
  final _choicesList = [
    {'name': 'Drama', 'select': false},
    {'name': 'Action', 'select': false},
    {'name': 'Animation', 'select': false},
    {'name': 'Sci-Fi', 'select': false},
    {'name': 'Horror', 'select': false},
  ];
  final TextEditingController titleController = TextEditingController();
  final TextEditingController directorController = TextEditingController();
  final TextEditingController summaryController = TextEditingController();

  @override
  void initState() {
    super.initState();
    stackRouter = context.router;
    id = widget.id;
    getMovieById();
  }

  void getMovieById() async {
    if (id != null) {
      movie = await getItProvider.get<MovieList>().getMovie(id!);
      titleController.text = movie!.title!;
      directorController.text = movie!.director!;
      summaryController.text = movie!.summary!;
      var geners = movie!.genres;
      for (var genre in geners!) {
        var index = getGenreIndex(genre.name);
        if (index >= 0) {
          _choicesList.elementAt(index)['select'] = true;
        }
      }
      setState(() {});
    }
  }

  int getGenreIndex(String name) {
    for (var i = 0; i < _choicesList.length; i++) {
      if (name == _choicesList.elementAt(i)['name']) {
        return i;
      }
    }
    return -1;
  }

  void saveMovie() {
    if (isGenreSelected().isEmpty) {
      message = 'Please select a genre.';
      var snackBar = SnackBar(
        content: Text(message),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }
    if (movie != null) {
      debugPrint("${movie!.id}");
      getItProvider.get<MovieList>().updateMovie(Movie(
          movie!.id,
          titleController.text,
          directorController.text,
          summaryController.text,
          isGenreSelected()));
    } else {
      var id = getItProvider.get<MovieList>().movies.length;
      getItProvider.get<MovieList>().addMovie(Movie(
          id + 1,
          titleController.text,
          directorController.text,
          summaryController.text,
          isGenreSelected()));
    }
    stackRouter!.navigateBack();
  }

  void deleteMovie() {
    if (movie != null) {
      debugPrint("${movie!.id}");
      getItProvider.get<MovieList>().removeMovie(movie!.id);
    }
    stackRouter!.navigateBack();
  }

  List<Genre> isGenreSelected() {
    List<Genre> genreList = [];
    for (var genre in _choicesList) {
      if (genre["select"] == true) {
        genreList.add(Genre(genre["name"].toString(), true));
      }
    }
    return genreList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          id != null
              ? InkWell(
                  child: const Padding(
                    padding: EdgeInsets.all(12),
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                  onTap: () => deleteMovie(),
                )
              : Container(),
          InkWell(
            child: const Padding(
              padding: EdgeInsets.all(12),
              child: Icon(
                Icons.check,
                color: Colors.white,
              ),
            ),
            onTap: () => saveMovie(),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextField(
                maxLines: 1,
                maxLength: 50,
                controller: titleController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal)),
                  labelText: 'Title',
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              TextField(
                maxLines: 1,
                maxLength: 50,
                controller: directorController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal)),
                  labelText: 'Director',
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              TextField(
                maxLines: 5,
                maxLength: 100,
                controller: summaryController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.teal),
                  ),
                  labelText: 'Summary',
                  alignLabelWithHint: true,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Text(
                "Genres",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 16),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Wrap(
                  spacing: 10.0, // gap between adjacent chips
                  runSpacing: 2.0, // gap between lines
                  children: List.generate(_choicesList.length, (index) {
                    return ChipWidget(_choicesList[index]);
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
