import 'package:pre_interview/models/genre.dart';

class Movie {
  int id = 0;
  String? title;
  String? director;
  String? summary;
  List<Genre>? genres;

  Movie(this.id, this.title, this.director, this.summary, this.genres);
}