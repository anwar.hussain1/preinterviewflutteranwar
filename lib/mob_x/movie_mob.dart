import 'package:mobx/mobx.dart';
import '../models/genre.dart';
part 'movie_mob.g.dart';

class Movie = _Movie with _$Movie;

abstract class _Movie with Store {
  @observable
  int id = 0;

  @observable
  String? title;

  @observable
  String? director;

  @observable
  String? summary;

  @observable
  List<Genre>? genres;

  _Movie(this.id, this.title, this.director, this.summary, this.genres);

}