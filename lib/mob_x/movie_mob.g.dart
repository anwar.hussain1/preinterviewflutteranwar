// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_mob.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$Movie on _Movie, Store {
  late final _$idAtom = Atom(name: '_Movie.id', context: context);

  @override
  int get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  late final _$titleAtom = Atom(name: '_Movie.title', context: context);

  @override
  String? get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String? value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  late final _$directorAtom = Atom(name: '_Movie.director', context: context);

  @override
  String? get director {
    _$directorAtom.reportRead();
    return super.director;
  }

  @override
  set director(String? value) {
    _$directorAtom.reportWrite(value, super.director, () {
      super.director = value;
    });
  }

  late final _$summaryAtom = Atom(name: '_Movie.summary', context: context);

  @override
  String? get summary {
    _$summaryAtom.reportRead();
    return super.summary;
  }

  @override
  set summary(String? value) {
    _$summaryAtom.reportWrite(value, super.summary, () {
      super.summary = value;
    });
  }

  late final _$genresAtom = Atom(name: '_Movie.genres', context: context);

  @override
  List<Genre>? get genres {
    _$genresAtom.reportRead();
    return super.genres;
  }

  @override
  set genres(List<Genre>? value) {
    _$genresAtom.reportWrite(value, super.genres, () {
      super.genres = value;
    });
  }

  @override
  String toString() {
    return '''
id: ${id},
title: ${title},
director: ${director},
summary: ${summary},
genres: ${genres}
    ''';
  }
}
