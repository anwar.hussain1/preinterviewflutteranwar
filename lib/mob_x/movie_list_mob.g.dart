// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list_mob.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieList on _MovieList, Store {
  late final _$moviesAtom = Atom(name: '_MovieList.movies', context: context);

  @override
  ObservableList<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(ObservableList<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$addMovieAsyncAction =
      AsyncAction('_MovieList.addMovie', context: context);

  @override
  Future<void> addMovie(Movie movie) {
    return _$addMovieAsyncAction.run(() => super.addMovie(movie));
  }

  late final _$getMovieAsyncAction =
      AsyncAction('_MovieList.getMovie', context: context);

  @override
  Future<Movie?> getMovie(int id) {
    return _$getMovieAsyncAction.run(() => super.getMovie(id));
  }

  late final _$updateMovieAsyncAction =
      AsyncAction('_MovieList.updateMovie', context: context);

  @override
  Future<void> updateMovie(Movie movieP) {
    return _$updateMovieAsyncAction.run(() => super.updateMovie(movieP));
  }

  late final _$removeMovieAsyncAction =
      AsyncAction('_MovieList.removeMovie', context: context);

  @override
  Future<void> removeMovie(int id) {
    return _$removeMovieAsyncAction.run(() => super.removeMovie(id));
  }

  @override
  String toString() {
    return '''
movies: ${movies}
    ''';
  }
}
