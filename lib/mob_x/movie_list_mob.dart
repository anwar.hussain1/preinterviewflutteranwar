import 'package:mobx/mobx.dart';
import 'package:pre_interview/mob_x/movie_mob.dart';

part 'movie_list_mob.g.dart';

class MovieList = _MovieList with _$MovieList;

abstract class _MovieList with Store {
  @observable
  ObservableList<Movie> movies = ObservableList<Movie>();

  @action
  Future<void> addMovie(Movie movie) async {
    movies.add(movie);
  }

  @action
  Future<Movie?> getMovie(int id) async {
    for (var i = 0; i < movies.length; i++) {
      var movie = movies[i];
      if (movie.id == id) {
        return movie;
      }
    }
    return null;
  }

  @action
  Future<void> updateMovie(Movie movieP) async {
    for (var i = 0; i < movies.length; i++) {
      var movie = movies[i];
      if (movie.id == movieP.id) {
        movie.title = movieP.title;
        movie.director = movieP.director;
        movie.summary = movieP.summary;
        movie.genres = movieP.genres;
      }
    }
  }

  @action
  Future<void> removeMovie(int id) async{
    for (var i = 0; i < movies.length; i++) {
      var movie = movies[i];
      if (movie.id == id) {
        movies.removeAt(i);
        break;
      }
    }
  }
}
