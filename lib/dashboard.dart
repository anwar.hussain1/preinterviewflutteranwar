import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:pre_interview/get_it/locator.dart';
import 'package:pre_interview/my_widgets/movie_list_item.dart';
import 'package:pre_interview/routes/app_router.gr.dart';

import 'mob_x/movie_list_mob.dart';
import 'mob_x/movie_mob.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  StackRouter? stackRouter;
  ObservableList<Movie>? list;
  ObservableList<Movie> _searchResult = ObservableList<Movie>();

  @override
  void initState() {
    super.initState();
    stackRouter = context.router;
    list = getItProvider.get<MovieList>().movies;
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    for (var movie in list!) {
      if (movie.title!.toLowerCase().contains(text.toLowerCase())) {
        _searchResult.add(movie);
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movies Collection"),
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: TextField(
                      onChanged: (text) {
                        onSearchTextChanged(text);
                      },
                      decoration: InputDecoration(
                        hintText: "Search by title...",
                        border: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black54),
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: _searchResult.isEmpty
                  ? Observer(
                      builder: (_) => ListView.builder(
                        shrinkWrap: true,
                        itemCount: list!.length,
                        padding: const EdgeInsets.only(bottom: 60, top: 10),
                        itemBuilder: (context, index) {
                          return MovieListItemWidget(movie: list![index]);
                        },
                      ),
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      itemCount: _searchResult.length,
                      padding: const EdgeInsets.only(bottom: 60),
                      itemBuilder: (context, index) {
                        return MovieListItemWidget(movie: _searchResult[index]);
                      },
                    ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.pink,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          stackRouter?.push(CreateMovieRoute());
        },
      ),
    );
  }
}
